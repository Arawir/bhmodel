#ifndef BASESTATE
#define BASESTATE

#include <cassert>

#include "basicmath.h"
#include "experimentconfig.h"

struct BaseState
{
    int mX1, mX2;
    uint mNumber;
    ExperimentConfig& mConfig;


    BaseState( int nX1, int nX2, uint nNumber, ExperimentConfig& nConfig) :
        mX1{ nX1 }
      , mX2{ nX2 }
      , mNumber{ nNumber }
      , mConfig{ nConfig }
    {
        //sprawdzic czy dodatnie
        assert(mX1<=mX2);
    }

    uint oneNodeSubstate(uint m1)
    {
        uint lParticlesInM1 = 0;
        if(mX1==m1){ lParticlesInM1++; }
        if(mX2==m1){ lParticlesInM1++; }

        return lParticlesInM1;
    }

    uint twoNodeSubstate(uint m1, uint m2)
    {
        uint lParticlesInM1 = 0;
        uint lParticlesInM2 = 0;
        if(mX1==m1){ lParticlesInM1++; }
        if(mX2==m1){ lParticlesInM1++; }
        if(mX1==m2){ lParticlesInM2++; }
        if(mX2==m2){ lParticlesInM2++; }
        return lParticlesInM1*3 + lParticlesInM2;
    }

    friend std::ostream& operator<<(std::ostream& os, const BaseState& mState);
};

std::ostream& operator<<(std::ostream& os, const BaseState& mState);

#endif // BASESTATE

