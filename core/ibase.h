#ifndef IBASE
#define IBASE

#include "basestate.h"

class iBase
{
public:
    iBase(){ }

    virtual ~iBase() = default;
    virtual void generate() = 0;
    virtual uint size() const = 0;
    virtual bool stateExists(int nX1, int nX2) const = 0;
    virtual BaseState& state(uint nNumber) = 0;
    virtual BaseState& state(int nX1, int nX2) = 0;
    virtual uint identifyStateNumber(int nX1, int nX2) const = 0;
   // virtual friend std::ostream& operator<<(std::ostream& os, const iBase& nBase) = 0;
};



#endif // IBASE

