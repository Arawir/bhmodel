#ifndef BASICPHYSICS_H
#define BASICPHYSICS_H

#include "base.h"

void fillHamiltonian(arma::mat& H, const Base& nBase, ExperimentConfig& nConfig);
void fillHamiltonian(arma::sp_mat& H, const Base& nBase, ExperimentConfig& nConfig);


#endif // BASICPHYSICS_H
