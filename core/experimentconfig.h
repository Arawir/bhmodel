#ifndef EXPERIMENTCONFIG
#define EXPERIMENTCONFIG

#include <armadillo>

struct ExperimentConfig
{
    uint mM=32;
    double mL = 1.0;
    bool mPeriodicBoundaries=true;
    double mParamTOMyLiebVerssion = 0.5;
    double mMiu = 0.0;
    double mNi = 0.0;

    double mC =1.0; //set by program

    double mDelta;
    double U;
    double J;
    double mPotChem;
    double mG;

};

#endif // EXPERIMENTCONFIG

