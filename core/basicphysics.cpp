#include "basicphysics.h"

void fillHamiltonian(arma::mat& H, const Base& nBase, ExperimentConfig& nConfig)
{
    for(auto& state : nBase.mStates){
        //U
        if(state.mX1 == state.mX2){
            H(state.mNumber, state.mNumber) += nConfig.U/2.0;
        }

        //miu
        H(state.mNumber, state.mNumber) += 2.0*nConfig.mPotChem;

        //+-
        if(state.mX1 == state.mX2){
            if(nBase.stateExists(state.mX1-1, state.mX2)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1-1, state.mX2)) += -2.0*nConfig.J;
            }
        }
        if(state.mX1 == (state.mX2-1)){
            if(nBase.stateExists(state.mX1, state.mX2-1)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1, state.mX2-1)) += -2.0*nConfig.J;
            }
        }
        if((state.mX1-state.mX2)>1){
            if(nBase.stateExists(state.mX1-1, state.mX2)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1-1, state.mX2)) += -1.0*nConfig.J;
            }

            if(nBase.stateExists(state.mX1, state.mX2-1)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1, state.mX2-1)) += -1.0*nConfig.J;
            }
        }


        //-+
        if(state.mX1 == state.mX2){
            if(nBase.stateExists(state.mX1, state.mX2+1)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1, state.mX2+1)) += -2.0*nConfig.J;
            }
        }
        if(state.mX1 == (state.mX2-1)){
            if(nBase.stateExists(state.mX1+1, state.mX2)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1+1, state.mX2)) += -2.0*nConfig.J;
            }
        }
        if((state.mX1-state.mX2)>1){
            if(nBase.stateExists(state.mX1, state.mX2+1)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1, state.mX2+1)) += -1.0*nConfig.J;
            }

            if(nBase.stateExists(state.mX1+1, state.mX2)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1+1, state.mX2)) += -1.0*nConfig.J;
            }
        }
    }
}



void fillHamiltonian(arma::sp_mat& H, const Base& nBase, ExperimentConfig& nConfig)
{
    for(auto& state : nBase.mStates){
        if(state.mX1 == state.mX2){ H(state.mNumber, state.mNumber) += nConfig.U/2.0; }

        //+-
        if(state.mX1 == state.mX2){
            if(nBase.stateExists(state.mX1-1, state.mX2)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1-1, state.mX2)) += -sqrt(2.0)*nConfig.J;
            }
        }
        if(state.mX1 == (state.mX2-1)){
            if(nBase.stateExists(state.mX1, state.mX2-1)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1, state.mX2-1)) += -sqrt(2.0)*nConfig.J;
            }
        }
        if((state.mX1-state.mX2)>1){
            if(nBase.stateExists(state.mX1-1, state.mX2)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1-1, state.mX2)) += -1.0*nConfig.J;
            }

            if(nBase.stateExists(state.mX1, state.mX2-1)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1, state.mX2-1)) += -1.0*nConfig.J;
            }
        }


        //-+
        if(state.mX1 == state.mX2){
            if(nBase.stateExists(state.mX1, state.mX2+1)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1, state.mX2+1)) += -sqrt(2.0)*nConfig.J;
            }
        }
        if(state.mX1 == (state.mX2-1)){
            if(nBase.stateExists(state.mX1+1, state.mX2)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1+1, state.mX2)) += -sqrt(2.0)*nConfig.J;
            }
        }
        if((state.mX1-state.mX2)>1){
            if(nBase.stateExists(state.mX1, state.mX2+1)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1, state.mX2+1)) += -1.0*nConfig.J;
            }

            if(nBase.stateExists(state.mX1+1, state.mX2)){
                H(state.mNumber, nBase.identifyStateNumber(state.mX1+1, state.mX2)) += -1.0*nConfig.J;
            }
        }
    }
}

