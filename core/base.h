#ifndef BASE
#define BASE

#include "experimentconfig.h"
#include "basestate.h"
#include "ibase.h"

class Base : public iBase
{
public:
    ExperimentConfig& mConfig;
    std::vector<BaseState> mStates;

public:
    Base(ExperimentConfig& nConfig) :
        iBase{}
      , mConfig{ nConfig }
    {

    }

    ~Base() = default;

    void generate()
    {
        uint lNumber=0;
        for(int i=0; i<mConfig.mM; i++){
            for(int j=i; j<mConfig.mM; j++){
                mStates.push_back( {i,j,lNumber, mConfig} );
                lNumber++;
            }
        }
    }

    uint size() const
    {
        return mStates.size();
    }

    bool stateExists(int nX1, int nX2) const
    {
        if( nX1<0 ){
            if(mConfig.mPeriodicBoundaries){
                nX1 = mConfig.mM + nX1;
            } else {
                return false;
            }
        }
        if( nX2<0 ){
            if(mConfig.mPeriodicBoundaries){
                nX2 = mConfig.mM + nX2;
            } else {
                return false;
            }
        }


        if( nX1>=mConfig.mM ){
            if(mConfig.mPeriodicBoundaries){
                nX1 = nX1 - mConfig.mM;
            } else {
                return false;
            }
        }
        if( nX2>=mConfig.mM ){
            if(mConfig.mPeriodicBoundaries){
                nX2 =  nX2 - mConfig.mM;
            } else {
                return false;
            }
        }

        for(auto& state : mStates){
            if( (state.mX1==nX1) && (state.mX2==nX2) ) return true;
            if( (state.mX1==nX2) && (state.mX2==nX1) ) return true;
        }
        return false;

    }

    BaseState& state(uint nNumber) override
    {
        assert(mStates.at(nNumber).mNumber == nNumber);
        return mStates.at(nNumber);
    }

    BaseState& state(int nX1, int nX2)
    {
        return state( identifyStateNumber(nX1, nX2) );
    }

    uint identifyStateNumber(int nX1, int nX2) const
    {
        if( nX1<0 ){
            assert(mConfig.mPeriodicBoundaries);
            nX1 = mConfig.mM + nX1;
        }
        if( nX2<0 ){
            assert(mConfig.mPeriodicBoundaries);
            nX2 = mConfig.mM + nX2;
        }

        if( nX1>=mConfig.mM ){
            assert(mConfig.mPeriodicBoundaries);
            nX1 = nX1 - mConfig.mM;
        }
        if( nX2>=mConfig.mM ){
            assert(mConfig.mPeriodicBoundaries);
            nX2 = nX2 - mConfig.mM;
        }


        for(auto& state : mStates){
            if( (state.mX1==nX1) && (state.mX2==nX2) ) return state.mNumber;
            if( (state.mX1==nX2) && (state.mX2==nX1) ) return state.mNumber;
        }

        assert(!"wrong state");
        return 0;
    }

    friend std::ostream& operator<<(std::ostream& os, const Base& nBase);
};

std::ostream& operator<<(std::ostream& os, const Base& nBase);

#endif // BASE

