#ifndef BASICMATH
#define BASICMATH

#include <iostream>
#include <armadillo>
#include <iomanip>
#include <complex>


#define im std::complex<double>{0.0,1.0}
#define output std::cout << std::fixed << std::setprecision(4)

typedef std::complex<double> cpl;

struct Eigen
{
    double mValue;
    arma::vec mVector;

    Eigen(double nValue, arma::vec nVector) :
        mValue{ nValue }
      , mVector{ nVector }
    {

    }
};


void calculateEigens(arma::mat nMatrix, std::vector<Eigen>& nEigens);
void calculateEigens(const arma::sp_mat& nMatrix, std::vector<Eigen>& nEigens, uint nEigensNumber);


#endif // BASICMATH

