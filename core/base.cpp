#include "base.h"

std::ostream& operator<<(std::ostream& os, const Base& nBase)
{
    for(auto& state : nBase.mStates){
        os << state << std::endl;
    }
    return os;
}
