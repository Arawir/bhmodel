#include "basestate.h"

std::ostream& operator<<(std::ostream& os, const BaseState& mState)
{
    for(uint m=0; m<mState.mConfig.mM; m++){
        if( (mState.mX1==m) && (mState.mX2==m) ){ os << '2'; }
        else if( (mState.mX1==m) || (mState.mX2==m) ){ os << '1'; }
        else{ os << '0'; }
    }
    return os;
}
