#include "basicmath.h"

void calculateEigens(arma::mat nMatrix, std::vector<Eigen>& nEigens)
{
    arma::mat lEigenvectors;
    arma::vec lEigenvalues;

    eig_sym(lEigenvalues, lEigenvectors, nMatrix);

    nEigens.clear();
    for(uint i=0; i<lEigenvalues.size(); i++){
        nEigens.push_back( {lEigenvalues.at(i), lEigenvectors.col(i)} );
    }
}

void calculateEigens(const arma::sp_mat& nMatrix, std::vector<Eigen>& nEigens, uint nEigensNumber)
{
    arma::mat lEigenvectors;
    arma::vec lEigenvalues;

    eigs_sym(lEigenvalues, lEigenvectors, nMatrix, nEigensNumber);

    for(uint i=0; i<lEigenvalues.size(); i++){
        nEigens.push_back( {lEigenvalues.at(i), lEigenvectors.col(i)} );
    }
}

