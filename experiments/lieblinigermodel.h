#ifndef LIEBLINIGERMODEL_H
#define LIEBLINIGERMODEL_H


#include "../core/basicmath.h"
#include "../core/basicphysics.h"

double f(ExperimentConfig& nConfig, double k);
double calculateK(ExperimentConfig& nConfig);
double liebLinigerEnergy(ExperimentConfig& nConfig);

#endif // LIEBLINIGERMODEL_H
