#include "lieblinigermodel.h"

double f(ExperimentConfig& nConfig, double k){
    double c = nConfig.mC;
    double ni = nConfig.mNi;
    return 2.0*atan(c/2.0/k) + M_PI*ni - k;
}

double calculateK(ExperimentConfig& nConfig)
{
    double lMinK = 0.0;
    double lMaxK = 1000.0;
    double lDeltaK = 1.0;

    while(lDeltaK>0.0001){
        double k=lMinK;
        while(k<lMaxK){
            if( (f(nConfig, k)*f(nConfig, k+lDeltaK))<0.0 ){
                lMinK = k;
                lMaxK = k+lDeltaK;
                break;
            }
            k += lDeltaK;
            //assert(k<lMaxK && "Cannot find k in selected range");
        }
        lDeltaK /= 10.0;
    }

    return (lMinK+lMaxK)/2.0;
}

double liebLinigerEnergy(ExperimentConfig& nConfig)
{
    double k = calculateK(nConfig);
    double miu = nConfig.mMiu;
    return 2.0*pow(k,2) + 2.0*pow(M_PI*miu,2);
    //return nConfig.mParamTOMyLiebVerssion*(2.0*lK*lK+2.0*M_PI*M_PI*nConfig.mMiu*nConfig.mMiu);
}
