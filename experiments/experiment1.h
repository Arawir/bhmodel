#ifndef EXPERIMENT1
#define EXPERIMENT1

#include "../operators/hamiltonian.h"
#include "lieblinigermodel.h"
#include "../operators/hamiltonianwithmomentum.h"

namespace Experiment1
{

//    void distanceHistorgam(Eigen& nEigen)
//    {
//        for(uint i
//    }

    //kontrola hamiltonianu
    void ex0()
    {
       ExperimentConfig lConfig;
       Base lBase{ lConfig };
       Hamiltonian lHamiltonian{ lConfig, lBase };

       lConfig.mM = 4;
       lConfig.mL = 1.0;
       lConfig.mDelta = lConfig.mL/lConfig.mM;

       lConfig.J = 1.0;
       lConfig.U = 0.0;
       lConfig.mPotChem = 2.0*lConfig.J;

       lConfig.mPeriodicBoundaries = true;

       lBase.generate();


       lHamiltonian.regenerate();
       lHamiltonian.recalculate();

        output << "Hamiltonian (J=1; U=0; Mi=2; M=4: " << std::endl
               << lHamiltonian.mMatrix << std::endl;


        output << lBase << std::endl;

        output << "Poziomy energetyczne i wektory stanu:" << std::endl;
        for(auto& eigen : lHamiltonian.mEigens)
        {
            output << "E= " << eigen.mValue << std::endl
                   << eigen.mVector
                   << std::endl;
        }

    }

    void ex1()
    {
       ExperimentConfig lConfig;
       Base lBase{ lConfig };
       Hamiltonian lHamiltonian{ lConfig, lBase };

       lConfig.J = 1.0;
       lConfig.U = 0.0;
       lConfig.mPotChem = 0.0;
       lConfig.mM = 32;
       lConfig.mPeriodicBoundaries = true;

       lBase.generate();
       lHamiltonian.regenerate();

      // output << lHamiltonian.mMatrix << std::endl;

       lHamiltonian.recalculate();

       for(auto& eigen : lHamiltonian.mEigens){
           output << eigen.mValue << std::endl;
       }
    }

    void ex2()
    {
       ExperimentConfig lConfig;
       Base lBase{ lConfig };
       Hamiltonian lHamiltonian{ lConfig, lBase };

       lConfig.mM = 32;
       lConfig.mL = 1.0;
       lConfig.mDelta = lConfig.mL/lConfig.mM;


       lConfig.mPeriodicBoundaries = true;

       lBase.generate();

       double lG = 0.0;
       while(lG<10.01){
           lConfig.J = 1.0/pow(lConfig.mDelta,2);
           //lConfig.J = 1.0;
           lConfig.U = lG/lConfig.mDelta;
           lConfig.mPotChem = 2.0*lConfig.J;

           lHamiltonian.regenerate();
           lHamiltonian.recalculate();
           output << lG << " "
                  << lHamiltonian.energy(0) << " "
                  << std::endl;
           lG += 0.1;
       }
    }

    void ex3()
    {
       ExperimentConfig lConfig;
       Base lBase{ lConfig };
       Hamiltonian lHamiltonian{ lConfig, lBase };

       lConfig.mM = 20;
       lConfig.mL = 1.0;
       lConfig.mDelta = lConfig.mL/lConfig.mM;




       lConfig.mPeriodicBoundaries = true;



       while(lConfig.mM < 31){
           lConfig.mDelta = lConfig.mL/lConfig.mM;
           lConfig.J = 1.0*pow(lConfig.mDelta, 2);
           lConfig.U = 1.0*pow(lConfig.mDelta,1);
           lConfig.mPotChem = 2.0*lConfig.J;

           lBase.generate();
           lHamiltonian.regenerate();
           lHamiltonian.recalculate();

           output << lConfig.mM << " "
                  << lConfig.U << " "
                  << lConfig.J << " "
                  << lHamiltonian.energy(0) << " "
                  << std::endl;
           lConfig.mM++;
       }
    }

    //lieb liniger energies
    void ex4()
    {
       ExperimentConfig config;

       config.mC = 1.0;
       config.mMiu = 0.0;
       config.mNi = 0.0;

       for(double g=0.1; g<=10.05; g+=0.1){
           config.mC = g/2.0;
           output << g << " ";
           output << liebLinigerEnergy(config) << " ";
           output << std::endl;
       }

    }

    void ex5()
    {
       ExperimentConfig config;
       Base base{ config };
       HamiltonianWithMomentum hamiltonianP{ config, base };

       config.mM = 4;
       config.mL = 1.0;
       config.mDelta = config.mL/config.mM;
       config.mPeriodicBoundaries = true;

       base.generate();
       hamiltonianP.regenerate();
       output << hamiltonianP.mMatrix;





    }


}
#endif // EXPERIMENT1

