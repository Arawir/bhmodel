#ifndef HAMILTONIANWITHMOMENTUM
#define HAMILTONIANWITHMOMENTUM

#include "../core/base.h"

class HamiltonianWithMomentum
{
public:
    ExperimentConfig& mConfig;
    Base& mBase;
    arma::cx_mat mMatrix;
    std::vector<Eigen> mEigens;

public:
    HamiltonianWithMomentum(ExperimentConfig& nConfig, Base& nBase) :
        mConfig{ nConfig }
      , mBase{ nBase }
      , mMatrix{ }
      , mEigens{ }
    {

    }
    ~HamiltonianWithMomentum() = default;

    void regenerate()
    {
        if(sqrt(mMatrix.size()) != mBase.size()){
            prepareBase();
        }
        mMatrix.zeros();
        for(auto& state : mBase.mStates){
            addState(state);
        }
        assert(mMatrix.is_hermitian());
    }

    void recalculate()
    {
//        mEigens.clear();
//        arma::cx_mat tEignenvectors;
//        arma::vec tEigenvalues;

//        eig_sym(tEigenvalues, tEignenvectors, mMatrix);

//        for(uint i=0; i<tEigenvalues.size(); i++){
//            mEigens.push_back( {tEigenvalues[i], tEignenvectors.col(i)} );
//        }
    }

    double energy(uint n)
    {
        return mEigens[n].mValue;
    }

private:
    void prepareBase()
    {
        mMatrix.resize(mBase.size(), mBase.size());
        mMatrix.zeros();
    }

    void addState(const BaseState& nState )
    {
//        //U
//        if(nState.mX1 == nState.mX2){
//            mMatrix(nState.mNumber, nState.mNumber) += mConfig.U;
//        }

//        //miu
//        mMatrix(nState.mNumber, nState.mNumber) += 2.0*mConfig.mPotChem;


//        //K
//        if(nState.mX1 == nState.mX2){
//            if(mBase.stateExists(nState.mX1-1, nState.mX2)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1-1, nState.mX2)) += -sqrt(2.0)*mConfig.J;
//            }
//            if(mBase.stateExists(nState.mX1, nState.mX2+1)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2+1)) += -sqrt(2.0)*mConfig.J;
//            }
//        } else if( (nState.mX1 == (nState.mX2-1)) ){
//            if(mBase.stateExists(nState.mX1-1, nState.mX2)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1-1, nState.mX2)) += -mConfig.J;
//            }
//            if(mBase.stateExists(nState.mX1+1, nState.mX2)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1+1, nState.mX2)) += -sqrt(2.0)*mConfig.J;
//            }
//            if(mBase.stateExists(nState.mX1, nState.mX2-1)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2-1)) += -sqrt(2.0)*mConfig.J;
//            }
//            if(mBase.stateExists(nState.mX1, nState.mX2+1)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2+1)) += -mConfig.J;
//            }
//        } else if( (nState.mX1==0)&&(nState.mX2==mConfig.mM-1) ){
//            if(mBase.stateExists(nState.mX1-1, nState.mX2)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1-1, nState.mX2)) += -sqrt(2.0)*mConfig.J;
//            }
//            if(mBase.stateExists(nState.mX1+1, nState.mX2)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1+1, nState.mX2)) += -mConfig.J;
//            }
//            if(mBase.stateExists(nState.mX1, nState.mX2-1)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2-1)) += -mConfig.J;
//            }
//            if(mBase.stateExists(nState.mX1, nState.mX2+1)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2+1)) += -sqrt(2.0)*mConfig.J;
//            }
//        } else {
//            if(mBase.stateExists(nState.mX1-1, nState.mX2)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1-1, nState.mX2)) += -mConfig.J;
//            }
//            if(mBase.stateExists(nState.mX1+1, nState.mX2)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1+1, nState.mX2)) += -mConfig.J;
//            }
//            if(mBase.stateExists(nState.mX1, nState.mX2-1)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2-1)) += -mConfig.J;
//            }
//            if(mBase.stateExists(nState.mX1, nState.mX2+1)){
//                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2+1)) += -mConfig.J;
//            }
//        }

        //P
        cpl p = -im/(2.0*mConfig.mDelta);
        if(nState.mX1 == nState.mX2){
            if(mBase.stateExists(nState.mX1, nState.mX2+1)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2+1)) += sqrt(2.0)*p;
            }
            if(mBase.stateExists(nState.mX1-1, nState.mX2)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1-1, nState.mX2)) += -sqrt(2.0)*p;
            }

        } else if( (nState.mX1 == (nState.mX2-1)) ){
            if(mBase.stateExists(nState.mX1-1, nState.mX2)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1-1, nState.mX2)) += -1.0*p;
            }
            if(mBase.stateExists(nState.mX1+1, nState.mX2)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1+1, nState.mX2)) += sqrt(2.0)*p;
            }
            if(mBase.stateExists(nState.mX1, nState.mX2-1)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2-1)) += -sqrt(2.0)*p;
            }
            if(mBase.stateExists(nState.mX1, nState.mX2+1)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2+1)) += 1.0*p;
            }
        } else if( (nState.mX1==0)&&(nState.mX2==mConfig.mM-1) ){
            if(mBase.stateExists(nState.mX1-1, nState.mX2)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1-1, nState.mX2)) += -sqrt(2.0)*p;
            }
            if(mBase.stateExists(nState.mX1+1, nState.mX2)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1+1, nState.mX2)) += 1.0*p;
            }
            if(mBase.stateExists(nState.mX1, nState.mX2-1)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2-1)) += -1.0*p;
            }
            if(mBase.stateExists(nState.mX1, nState.mX2+1)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2+1)) += sqrt(2.0)*p;
            }
        } else {
            if(mBase.stateExists(nState.mX1-1, nState.mX2)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1-1, nState.mX2)) += -1.0*p;
            }
            if(mBase.stateExists(nState.mX1+1, nState.mX2)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1+1, nState.mX2)) += 1.0*p;
            }
            if(mBase.stateExists(nState.mX1, nState.mX2-1)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2-1)) += -1.0*p;
            }
            if(mBase.stateExists(nState.mX1, nState.mX2+1)){
                mMatrix(nState.mNumber, mBase.identifyStateNumber(nState.mX1, nState.mX2+1)) += 1.0*p;
            }
        }


    }
};

#endif // HAMILTONIANWITHMOMENTUM

